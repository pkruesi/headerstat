# headerstat

headerstat is a small utility which will try to connect, with http(s),
to domains in a domainlists file. It will connect concurrent to a given amount of domains (default 100 per cpucore).
It will then try to make a small statistic about the http response codes, headers, etc. depending on the set parameters.

- why it did this tool

  headerstat is a samll project which I use to learn the go programing language.
  It helped me to have a small project where I can use different features from go like go functions, channels, reading a file, unsing flag, etc..
  I tried to make it as concurent as possible.

## Download, build and run it

```bash
git clone https://gitlab.com/pkruesi/headerstat.git
cd headerstat/

go build .
./headerstat -f domainlists/100-domains
```

### print help

```bash
./headerstat -h
Usage of ./headerstat:
  -b int
      size of the channel's buffer (should be bigger then concurrent fetsching (default 480)
  -c int
      concurrent fetching (default 100 connections per cpucore) (default 400)
  -f string
      file with a list of domains, each line a domain
  -p string
      specify http or https (default "https")
  -s string
      defines the searchstring for which http header is searched
  -t int
      timeout for the http requests of the http.client (default 5)
  -v  shows connection results during runtime
  -vE
      connection Errors - shows a list of all connection Errors
  -vH
      Http codes - shows a list of all resceived Http response codes
  -vh
      all headers - shows a summary for all http headers which we got as response
  -vhv
      header values - shows the set values from the searched header
```

### examples

- check in a list of domains if a given header is set

  ```bash
  ./headerstat -f domainlists/10-domains -s "content-type"
  summary
    checked domains: 10
  
    search result for header: content-type
      "content-type" header found on 8 domains
      "content-type" is set for 80.000000% on the searched 10 domains
  ```

- check in a list of domains if a given header is set - show the values of the found header

  ```bash
  ./headerstat -f domainlists/10-domains -s "content-type" -vhv
  summary
    checked domains: 10
    http headers values for: content-type
      1     : text/html;charset=iso-8859-1
      2     : text/plain;charset=utf-8
      2     : text/html
      3     : text/html;charset=utf-8
  
    search result for header: content-type
      "content-type" header found on 8 domains
      "content-type" is set for 80.000000% on the searched 10 domains
  ```

- get all headers from a list of domains

  ```bash
  ./headerstat -f domainlists/10-domains -vh
  summary
    checked domains: 10
    http headers overview:
      1     : X-Content-Type-Options
      1     : X-Dns-Prefetch-Control
      1     : Content-Security-Policy
      1     : Alt-Svc
      1     : X-Xss-Protection
      1     : Cf-Cache-Status
      1     : X-Frame-Options
      1     : Accept-Ranges
      1     : Cross-Origin-Opener-Policy
      1     : Last-Modified
      1     : Cf-Ray
      1     : Nel
      1     : Age
      1     : Referrer-Policy
      1     : Report-To
      1     : X-Permitted-Cross-Domain-Policies
      1     : Etag
      1     : X-Download-Options
      1     : X-Powered-By
      1     : Cross-Origin-Resource-Policy
      1     : Set-Cookie
      1     : Upgrade
      2     : X-Amz-Cf-Id
      2     : X-Via
      2     : Via
      2     : X-Amz-Cf-Pop
      2     : Expect-Ct
      2     : X-Cache
      2     : X-Cdn-Cache-Status
      3     : Vary
      3     : Content-Length
      3     : Cache-Control
      3     : Strict-Transport-Security
      4     : Server
      5     : Location
      8     : Content-Type
      8     : Date
  ```

- get all Http status codes

  ```bash
  ./headerstat -f domainlists/10-domains -vH
  summary
    checked domains: 10
    http return codes:
      1     : 200 OK
      1     : 302 Found
      2     : 403 Forbidden
      2     : 0 
      4     : 301 Moved Permanently
  ```

- get all errors during connection

  ```bash
  ./headerstat -f domainlists/domains -vE
  summary
    checked domains: 10
    connection states:
      2     : dial tcp: lookup: no such host
      8     : http OK: connected: got a result
  ```

## From where to the domainlists

- [Switch open-data](https://www.switch.ch/open-data/)
- [Cisco umbrella](http://s3-us-west-1.amazonaws.com/umbrella-static/index.html)

## ToDo

- make it possible to define if the headerstat tool should fallow http redirects or not
- start to implement go test
- pack the tool inside a docker container
- etc.
