package main

import (
	"bufio"
	"context"
	"flag"
	"fmt"
	"log"
	"net/http"
	"os"
	"runtime"
	"strings"
	"time"

	"headerstat/data"
	"headerstat/utils"
)

var (
	domainsFile       string
	seconds           int
	bufferSize        int
	concurrent        int
	protocol          string
	runInVerbose      bool
	showHeaderSummary bool
	showHeaderValues  bool
	showErrorSummary  bool
	showHttpSummary   bool
	searchHeader      string
)

func init() {

	concurrentConnections := 100 * runtime.NumCPU()
	buffSize := int(float32(concurrentConnections) * 1.2)

	// mandotory options
	flag.StringVar(&domainsFile, "f", "", "file with a list of domains, each line a domain")

	// optional options
	//  configure the inner working
	flag.IntVar(&seconds, "t", 5, "timeout for the http requests of the http.client")
	flag.StringVar(&protocol, "p", "https", "specify http or https")
	flag.IntVar(&bufferSize, "b", buffSize, "size of the channel's buffer (should be bigger then concurrent fetsching")
	flag.IntVar(&concurrent, "c", concurrentConnections, "concurrent fetching (default 100 connections per cpucore)")

	//  how the output should get displayed
	flag.BoolVar(&runInVerbose, "v", false, "shows connection results during runtime")
	flag.BoolVar(&showHeaderSummary, "vh", false, "all headers - shows a summary for all http headers which we got as response")
	flag.BoolVar(&showHeaderValues, "vhv", false, "header values - shows the set values from the searched header")
	flag.BoolVar(&showHttpSummary, "vH", false, "Http codes - shows a list of all resceived Http response codes")
	flag.BoolVar(&showErrorSummary, "vE", false, "connection Errors - shows a list of all connection Errors")

	//  search options
	flag.StringVar(&searchHeader, "s", "", "defines the searchstring for which http header is searched")

	flag.Parse()
}

func main() {
	dCh := make(chan string, bufferSize)
	rCh := make(chan data.Result)

	file, err := os.Open(domainsFile)
	if err != nil {
		log.Panic(err)
	}
	defer file.Close()

	reader := bufio.NewReader(file)
	numLines, err := utils.LineCounter(reader)
	if err != nil {
		log.Panic(err)
	}

	// go to the start of the file
	file.Seek(0, 0)
	scanner := bufio.NewScanner(reader)

	// filling the dCh channel
	go func() {
		for scanner.Scan() {
			dCh <- scanner.Text()
		}
	}()

	ctx := context.Background()
	rConfig := utils.RequestConfig{
		Timeout:        time.Duration(seconds) * time.Second,
		Protocol:       protocol,
		Ctx:            ctx,
		FollowRedirect: false,
	}
	rConfig.NewRequestConfig()

	// running our worker getHeaders
	for i := 0; i < concurrent; i++ {
		go utils.GetHeaders(dCh, rCh)
	}

	errorMap := make(map[string]int)
	statusMap := make(map[int]int)
	headerMap := make(map[string]int)
	foundHeaderMap := make(map[string]int)

	// get the results
	for i := 0; i < numLines; i++ {
		result := <-rCh

		// output during runtime
		if runInVerbose {
			fmt.Printf("%-6d [%3d] %s, (%s)\n", i, result.Status, result.Domain, utils.GeneralizeError(result.Error).Error())
		}

		// count stuff for the statistics
		err = utils.GeneralizeError(result.Error)
		errorMap[err.Error()]++
		statusMap[result.Status]++
		for header := range result.Headers {
			headerMap[header]++
		}

		if searchHeader != "" {
			headerValues := result.Headers.Values(searchHeader)
			// ToDo
			// - may be here we need some more optimization
			values := strings.ToLower(strings.Join(headerValues, ","))
			val := strings.ReplaceAll(values, " ", "")
			if values != "" {
				foundHeaderMap[val]++
			}
		}
	}

	// output statistics
	// ToDo
	// - should be in its owen package
	fmt.Println("summary")
	fmt.Println("  checked domains:", numLines)

	if showErrorSummary {
		fmt.Println("  connection states:")
		for _, p := range utils.SortMapStringIntByValue(errorMap) {
			fmt.Printf("    %-6d: %s\n", p.Value, p.Key)
		}
	}

	if showHttpSummary {
		fmt.Println("  http return codes:")
		for _, p := range utils.SortMapIntIntByValue(statusMap) {
			fmt.Printf("    %-6d: %d %s\n", p.Value, p.Key, http.StatusText(p.Key))
		}
	}

	if showHeaderSummary {
		fmt.Println("  http headers overview:")
		for _, p := range utils.SortMapStringIntByValue(headerMap) {
			fmt.Printf("    %-6d: %s\n", p.Value, p.Key)
		}

	}

	if searchHeader != "" {
		hSet := 0
		if showHeaderValues {
			fmt.Println("  http headers values for:", searchHeader)
		}
		//for value, count := range foundHeaderMap {
		for _, p := range utils.SortMapStringIntByValue(foundHeaderMap) {
			if showHeaderValues {
				fmt.Printf("    %-6d: %s\n", p.Value, p.Key)
			}

			hSet += p.Value
		}

		fmt.Println()
		fmt.Println("  search result for header:", searchHeader)
		fmt.Printf("    \"%s\" header found on %d domains\n", searchHeader, hSet)
		procent := 100.0 / float64(numLines) * float64(hSet)
		fmt.Printf("    \"%s\" is set for %f%% on the searched %d domains\n", searchHeader, procent, numLines)
	}

	close(dCh)
	close(rCh)
}
