package data

import "net/http"

// data transfer for the result
type Result struct {
	Domain  string
	Headers http.Header
	Status  int
	Error   error
}
