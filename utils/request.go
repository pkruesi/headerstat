package utils

import (
	"context"
	"crypto/tls"
	"fmt"
	"log"
	"net"
	"net/http"
	"time"

	"headerstat/data"
)

type RequestConfig struct {
	Timeout        time.Duration
	Protocol       string
	Ctx            context.Context
	FollowRedirect bool
}

var rConfig *RequestConfig

// sets the new request config
func (rc RequestConfig) NewRequestConfig() {
	rConfig = &rc
}

// iterate through the domains channel an get the headers and fill the result channel
func GetHeaders(dCh chan string, resCh chan data.Result) {
	for d := range dCh {
		resCh <- doRequest(d)
	}
}

// do the http request and return a data.Result set
func doRequest(d string) data.Result {
	var res data.Result
	res.Domain = d

	tr := &http.Transport{
		MaxIdleConns: 10,
		Dial: (&net.Dialer{
			Timeout:   rConfig.Timeout,
			KeepAlive: rConfig.Timeout,
		}).Dial,
		DisableKeepAlives:     true,
		IdleConnTimeout:       rConfig.Timeout,
		ResponseHeaderTimeout: rConfig.Timeout,
		TLSClientConfig:       &tls.Config{InsecureSkipVerify: true},
		TLSHandshakeTimeout:   rConfig.Timeout,
	}

	client := &http.Client{
		Transport: tr,
		CheckRedirect: func(req *http.Request, via []*http.Request) error {
			return http.ErrUseLastResponse
		},
	}


	// the context is alloud to life 1 second longer then the given timeout
	ctx, cancelDeadline := context.WithDeadline(rConfig.Ctx, time.Now().Add(rConfig.Timeout+1))
	defer cancelDeadline()
	myurl := fmt.Sprintf("%s://%s", rConfig.Protocol, d)

	req, err := http.NewRequest(http.MethodGet, myurl, nil)
	if err != nil {
		log.Fatal(err)
	}

	resp, err := client.Do(req.WithContext(ctx))
	if err != nil {
		res.Error = err
	} else {
		res.Headers = resp.Header
		err = fmt.Errorf("http: %w", fmt.Errorf("OK"))
		res.Error = err
		res.Status = resp.StatusCode
		resp.Body.Close()
	}
	tr.CloseIdleConnections()
	ctx.Done()

	return res
}
