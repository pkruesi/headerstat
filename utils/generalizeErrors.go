package utils

import (
	"fmt"
	"regexp"
)

// rewrite the error messages
// so that we do not have ip's, domainnames, ports etc. in the error message
// this helps to make some statistics with this error messages
func GeneralizeError(err error) error {

	// positiv connection - not realy an error
	r, _ := regexp.Compile("OK")
	if r.MatchString(err.Error()) {
		return fmt.Errorf("http OK: connected: got a result")
	}

	r, _ = regexp.Compile("response missing Location header")
	if r.MatchString(err.Error()) {
		return fmt.Errorf("301 / 302 / 303 redirect but missing Location header")
	}

	r, _ = regexp.Compile("Get .*: failed to parse Location header .* invalid character \".*\" in host name")
	if r.MatchString(err.Error()) {
		return fmt.Errorf("failed to parse Location header: invalid character in host name")
	}


	r, _ = regexp.Compile("Get .* malformed HTTP status code")
	if r.MatchString(err.Error()) {
		return fmt.Errorf("malformed HTTP status code")
	}

	r, _ = regexp.Compile("Get .* local error: tls: unexpected message")
	if r.MatchString(err.Error()) {
		return fmt.Errorf("local error: tls: unexpected")
	}


	// lookup / dns related
	r, _ = regexp.Compile("dial tcp: lookup.*on.*: no such host")
	if r.MatchString(err.Error()) {
		return fmt.Errorf("dial tcp: lookup: dns no such host")
	}

	r, _ = regexp.Compile("dial tcp: lookup.*: no such host")
	if r.MatchString(err.Error()) {
		return fmt.Errorf("dial tcp: lookup: no such host")
	}

	r, _ = regexp.Compile("dial tcp: lookup.*: server misbehaving")
	if r.MatchString(err.Error()) {
		return fmt.Errorf("dial tcp: lookup: server misbehaving")
	}

	r, _ = regexp.Compile("dial tcp: lookup .* on .*: read udp .*: i/o timeout")
	if r.MatchString(err.Error()) {
		return fmt.Errorf("dial tcp: lookup: read udp i/o timeout")
	}


	// connection / transport related 
	r, _ = regexp.Compile("read tcp .*\\->.*: read: connection reset by peer$")
	if r.MatchString(err.Error()) {
		return fmt.Errorf("read tcp: read: connection reset by peer")
	}

	r, _ = regexp.Compile("write tcp .*\\->.*: write: connection reset by peer$")
	if r.MatchString(err.Error()) {
		return fmt.Errorf("write tcp: write: connection reset by peer")
	}

	r, _ = regexp.Compile("connect: connection reset by peer")
	if r.MatchString(err.Error()) {
		return fmt.Errorf("connect: connection reset by peer")
	}

	r, _ = regexp.Compile("dial tcp .*:.*: connect: connection refused$")
	if r.MatchString(err.Error()) {
		return fmt.Errorf("dial tcp: connect: connection refused")
	}

	r, _ = regexp.Compile("dial tcp .*: connect: no route to host")
	if r.MatchString(err.Error()) {
		return fmt.Errorf("dial tcp: connect: no route to host")
	}

	r, _ = regexp.Compile("dial tcp .*: connect: invalid argument")
	if r.MatchString(err.Error()) {
		return fmt.Errorf("dial tcp: connect: invalid argument")
	}

	r, _ = regexp.Compile("dial tcp .*: connect: network is unreachable")
	if r.MatchString(err.Error()) {
		return fmt.Errorf("dial tcp: connect: network is unreachable")
	}

	r, _ = regexp.Compile("dial tcp .*: connect: permission denied")
	if r.MatchString(err.Error()) {
		return fmt.Errorf("dial tcp: connect: permission denied")
	}


	// tls errors
	r, _ = regexp.Compile("tls: received unexpected handshake message")
	if r.MatchString(err.Error()) {
		return fmt.Errorf("tls: received unexpected handshake message")
	}

	r, _ = regexp.Compile("remote error: tls: handshake failure")
	if r.MatchString(err.Error()) {
		return fmt.Errorf("remote error: tls: handshake failure")
	}

	r, _ = regexp.Compile("remote error: tls: unrecognized name")
	if r.MatchString(err.Error()) {
		return fmt.Errorf("remote error: tls: unrecognized name")
	}

	r, _ = regexp.Compile("remote error: tls: internal error")
	if r.MatchString(err.Error()) {
		return fmt.Errorf("remote error: tls: internal error")
	}

	r, _ = regexp.Compile("remote error: tls: no renegotiation")
	if r.MatchString(err.Error()) {
		return fmt.Errorf("remote error: tls: no renegotiation")
	}

	r, _ = regexp.Compile("local error: tls: no renegotiation")
	if r.MatchString(err.Error()) {
		return fmt.Errorf("local error: tls: no renegotiation")
	}

	r, _ = regexp.Compile("local error: tls: unexpected message")
	if r.MatchString(err.Error()) {
		return fmt.Errorf("local error: tls: unexpected message")
	}

	r, _ = regexp.Compile("tls: first record does not look like a TLS handshake")
	if r.MatchString(err.Error()) {
		return fmt.Errorf("tls: first record does not look like a TLS handshake")
	}

	r, _ = regexp.Compile("tls: : handshake message of length [0-9]* bytes exceeds maximum of 65536 bytes")
	if r.MatchString(err.Error()) {
		return fmt.Errorf("tls: handshake message exeeds length of maximum 65536 bytes")
	}

	r, _ = regexp.Compile("tls: invalid signature by the server certificate: crypto/rsa: verification error")
	if r.MatchString(err.Error()) {
		return fmt.Errorf("tls: invalid signature by the server certificate: crypto/rsa: verification error")
	}

	r, _ = regexp.Compile("tls: server selected unsupported protocol version")
	if r.MatchString(err.Error()) {
		return fmt.Errorf("tls: server selected unsupported protocol version")
	}


	// net/http related
	r, _ = regexp.Compile("net/http:.*transport connection broken: malformed HTTP status code")
	if r.MatchString(err.Error()) {
		return fmt.Errorf("net/http: HTTP/1.x transport connection broken: malformed HTTP status code")
	}

	r, _ = regexp.Compile("net/http:.*transport connection broken: malformed MIME header")
	if r.MatchString(err.Error()) {
		return fmt.Errorf("net/http: HTTP/1.x transport connection broken: malformed MIME header")
	}

	r, _ = regexp.Compile("net/http: .*transport connection broken: too many transfer encodings")
	if r.MatchString(err.Error()) {
		return fmt.Errorf("net/http: HTTP/1.x transport connection broken: too many transfer encodings")
	}

	r, _ = regexp.Compile("net/http: timeout awaiting response headers")
	if r.MatchString(err.Error()) {
		return fmt.Errorf("net/http: HTTP/1.x transport connection broken: too many transfer encodings")
	}

	r, _ = regexp.Compile("net/http: .* transport connection broken: unexpected EOF")
	if r.MatchString(err.Error()) {
		return fmt.Errorf("net/http: transport connection broken: unexpected EOF")
	}

	r, _ = regexp.Compile("Get: .*: net/http: TLS handshake timeout")
	if r.MatchString(err.Error()) {
		return fmt.Errorf("net/http: TLS handshake timeout")
	}

	r, _ = regexp.Compile("Get .*: failed to parse Location header \".*\": parse \".*\": net/url: invalid control character in URL")
	if r.MatchString(err.Error()) {
		return fmt.Errorf("net/url: failed to parse Location header: net/url: invalid control character in URL")
	}


	// EOF related
	r, _ = regexp.Compile(": EOF")
	if r.MatchString(err.Error()) {
		return fmt.Errorf("got EOF")
	}

	r, _ = regexp.Compile("Get: .* unexpected EOF")
	if r.MatchString(err.Error()) {
		return fmt.Errorf("unexpected EOF")
	}


	// http related
	r, _ = regexp.Compile("http: server gave HTTP response to HTTPS client")
	if r.MatchString(err.Error()) {
		return fmt.Errorf("http: server gave HTTP response to HTTPS client")
	}

	r, _ = regexp.Compile("Get .* after host")
	if r.MatchString(err.Error()) {
		return fmt.Errorf("http: Got something strange after host header")
	}


	// timeout related
	r, _ = regexp.Compile(": context deadline exceeded")
	if r.MatchString(err.Error()) {
		return fmt.Errorf("timeout: we run in a given timeout / deadline")
	}

	r, _ = regexp.Compile(": dial tcp.*: i/o timeout")
	if r.MatchString(err.Error()) {
		return fmt.Errorf("dial tcp: i/o timeout")
	}

	return err
}
