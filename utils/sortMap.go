package utils

import "sort"

// A data structure to hold a key/value (string/int) pair.
type PairStringInt struct{
	Key string
	Value int
}

// A slice of Pairs that implements sort.Interface to sort by Value.
type PairStringIntList []PairStringInt

func (p PairStringIntList) Swap(i, j int) { p[i], p[j] = p[j], p[i] }
func (p PairStringIntList) Len() int { return len(p) }
func (p PairStringIntList) Less(i, j int) bool { return p[i].Value < p[j].Value }

// A function to turn a map into a PairStringIntList, then sort and return it. 
func SortMapStringIntByValue(m map[string]int) PairStringIntList {
   p := make(PairStringIntList, len(m))
   i := 0
   for k, v := range m {
      p[i] = PairStringInt{k, v}
			i++
   }
   sort.Sort(p)
   return p
}

// A data structure to hold a key/value (int/int) pair.
type PairIntInt struct{
	Key int
	Value int
}

// A slice of Pairs that implements sort.Interface to sort by Value.
type PairIntIntList []PairIntInt

func (p PairIntIntList) Swap(i, j int) { p[i], p[j] = p[j], p[i] }
func (p PairIntIntList) Len() int { return len(p) }
func (p PairIntIntList) Less(i, j int) bool { return p[i].Value < p[j].Value }

// A function to turn a map into a PairIntIntList, then sort and return it. 
func SortMapIntIntByValue(m map[int]int) PairIntIntList {
   p := make(PairIntIntList, len(m))
   i := 0
   for k, v := range m {
      p[i] = PairIntInt{k, v}
			i++
   }
   sort.Sort(p)
   return p
}
